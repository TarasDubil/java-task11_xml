<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <div style="background-color: green; color: black;">
                    <h2>Gemstones</h2>
                </div>
                <table border="3">
                    <tr bgcolor="#2E9AFE">
                        <th>Name</th>
                        <th>Preciousness</th>
                        <th>Origin</th>
                        <th>Value</th>
                        <th>Colour</th>
                        <th>WayOfCutting</th>
                        <th>Transparency</th>
                    </tr>

                    <xsl:for-each select="gemstones/gemstone">

                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="preciousness"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="value"/></td>
                            <td><xsl:value-of select="visualParameters/colour"/></td>
                            <td><xsl:value-of select="visualParameters/wayOfCutting"/></td>
                            <td><xsl:value-of select="visualParameters/transparency"/></td>
                        </tr>

                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>