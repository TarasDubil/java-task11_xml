package com.dubilok.model;

public class VisualParameters {
    private String color;
    private int wayOfCutting;
    private int transparency;

    public VisualParameters() {
    }

    public VisualParameters(String color, int wayOfCutting, int transparency) {
        this.color = color;
        this.wayOfCutting = wayOfCutting;
        this.transparency = transparency;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWayOfCutting() {
        return wayOfCutting;
    }

    public void setWayOfCutting(int wayOfCutting) {
        this.wayOfCutting = wayOfCutting;
    }

    public int getTransparency() {
        return transparency;
    }

    public void setTransparency(int transparency) {
        this.transparency = transparency;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "color='" + color + '\'' +
                ", wayOfCutting=" + wayOfCutting +
                ", transparency=" + transparency +
                '}';
    }
}
