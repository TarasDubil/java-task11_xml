package com.dubilok.parser.stax;

import com.dubilok.model.GemStone;
import com.dubilok.model.VisualParameters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXReader {

    private static Logger logger = LogManager.getLogger(StAXReader.class);

    public static List<GemStone> parseGestones(File xml, File xsd) {
        List<GemStone> gemstoneList = new ArrayList<>();
        GemStone gemstone = null;
        VisualParameters visualParams = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "gemstone":
                            gemstone = new GemStone();
                            Attribute idAttr = startElement.getAttributeByName(new QName("gemstoneNo"));
                            if (idAttr != null) {
                                gemstone.setGemstoneNo(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gemstone != null;
                            gemstone.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "preciousness":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gemstone != null;
                            gemstone.setPreciousness(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "origin":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gemstone != null;
                            gemstone.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        case "value":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gemstone != null;
                            gemstone.setValue(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "visualParameters":
                            xmlEvent = xmlEventReader.nextEvent();
                            visualParams = new VisualParameters();
                            break;
                        case "colour":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert visualParams != null;
                            visualParams.setColor(xmlEvent.asCharacters().getData());
                            break;
                        case "wayOfCutting":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert visualParams != null;
                            visualParams.setWayOfCutting(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "transparency":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert visualParams != null;
                            visualParams.setTransparency(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                    }
                }
                endElement(gemstoneList, gemstone, visualParams, xmlEvent);
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            logger.error(e);
        }
        return gemstoneList;
    }

    private static void endElement(List<GemStone> gemstoneList, GemStone gemstone,
                                   VisualParameters visualParams, XMLEvent xmlEvent) {
        if (xmlEvent.isEndElement()) {
            EndElement endElement = xmlEvent.asEndElement();
            if (endElement.getName().getLocalPart().equals("gemstone")) {
                gemstone.setVisualParameters(visualParams);
                gemstoneList.add(gemstone);
            }
        }
    }


}
