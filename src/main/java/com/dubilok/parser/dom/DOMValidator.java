package com.dubilok.parser.dom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class DOMValidator {

    private static Logger logger = LogManager.getLogger(DOMDocCreator.class);

    public static Schema createSchema(File xsd) {
        Schema schema = null;
        try {
            // create a SchemaFactory capable of understanding WXS schemas
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            // load a WXS schema, represented by a Schema instance
            schema = factory.newSchema(xsd);
        } catch (Exception e) {
            logger.error(e);
        }
        return schema;
    }

    public static void validate(Schema schema, Document xml) throws IOException, SAXException {
        Validator validator = schema.newValidator();
        validator.validate(new DOMSource(xml));
    }

}
