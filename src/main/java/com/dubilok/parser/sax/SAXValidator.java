package com.dubilok.parser.sax;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

public class SAXValidator {

    private static Logger logger = LogManager.getLogger(SAXValidator.class);

    public static Schema createSchema(File xsd) {
        Schema schema = null;
        try {
            String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
            SchemaFactory factory = SchemaFactory.newInstance(language);
            schema = factory.newSchema(xsd);
        } catch (SAXException ex) {
            logger.error(ex);
        }
        return schema;
    }
}
