package com.dubilok.parser.sax;

import com.dubilok.model.GemStone;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SAXParserUser {

    private static Logger logger = LogManager.getLogger(SAXParserUser.class);
    private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

    public static List<GemStone> parseGemstones(File xml, File xsd) {
        List<GemStone> gemstoneList = new ArrayList<>();
        try {
            saxParserFactory.setSchema(SAXValidator.createSchema(xsd));
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SAXHandler saxHandler = new SAXHandler();
            saxParser.parse(xml, saxHandler);
            gemstoneList = saxHandler.getGemstoneList();
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            logger.error(ex);
        }
        return gemstoneList;
    }
}
