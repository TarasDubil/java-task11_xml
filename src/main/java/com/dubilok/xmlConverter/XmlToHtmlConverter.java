package com.dubilok.xmlConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

public class XmlToHtmlConverter {

    private static Logger logger = LogManager.getLogger(XmlToHtmlConverter.class);

    public static void convertXMLToHTML(Source xml, Source xslt) {
        StringWriter sw = new StringWriter();
        try {
            FileWriter fw = new FileWriter("src\\main\\resources\\xml\\gemstones.html");
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(xslt);
            transformer.transform(xml, new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();
            logger.info("devices.html generated successfully.");
        } catch (IOException | TransformerFactoryConfigurationError | TransformerException e) {
            logger.error(e);
        }
    }
}
