package com.dubilok.controller;

public interface Controller {
    void showDOM();

    void showSAX();

    void showStAX();

    void convertFromXMLToHTML();
}
