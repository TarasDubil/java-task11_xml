package com.dubilok.controller;

import com.dubilok.model.GemStone;
import com.dubilok.model.GemsStoneComparator;
import com.dubilok.parser.dom.DOMParserUser;
import com.dubilok.parser.sax.SAXParserUser;
import com.dubilok.parser.stax.StAXReader;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.util.List;

import static com.dubilok.xmlConverter.XmlToHtmlConverter.convertXMLToHTML;

public class ControllerImpl implements Controller {

    private static Logger logger = LogManager.getLogger(ControllerImpl.class);
    private File xml = new File("src\\main\\resources\\xml\\gemstonesXML.xml");
    private File xsd = new File("src\\main\\resources\\xml\\gemstonesXSD.xsd");
    private File xslt = new File("src\\main\\resources\\xml\\gemstonesXSLT.xsl");

    @Override
    public void showDOM() {
        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            printList(DOMParserUser.getGemstoneList(xml, xsd), "DOM");
        }
    }

    @Override
    public void showSAX() {
        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            printList(SAXParserUser.parseGemstones(xml, xsd), "SAX");
        }
    }

    @Override
    public void showStAX() {
        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            printList(StAXReader.parseGestones(xml, xsd), "StAX");
        }
    }

    @Override
    public void convertFromXMLToHTML() {
        Source xml = new StreamSource(this.xml);
        Source xslt = new StreamSource(this.xslt);
        convertXMLToHTML(xml, xslt);
    }

    private static boolean checkIfXSD(File xsd) {
        return xsd.isFile() && FilenameUtils.getExtension(xsd.getName()).equals("xsd");
    }

    private static boolean checkIfXML(File xml) {
        return xml.isFile() && FilenameUtils.getExtension(xml.getName()).equals("xml");
    }

    private static void printList(List<GemStone> gemstones, String parserName) {
        gemstones.sort(new GemsStoneComparator());
        logger.info(parserName);
        for (GemStone gemstone : gemstones) {
            logger.info(gemstone);
        }
    }
}
